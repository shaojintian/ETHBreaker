Required Python < 3.10

#  ETHBreaker by VolkiTech

## Version 1
Register https://infura.io to get free WEB3_INFURA_PROJECT_ID and WEB3_INFURA_API_SECRET for base.env file

## Version 2 (new)
### - No Infura required
### - Can be run in multiple threads
### - It is possible to use a proxy
### - You may set number of child wallets

![Screenshot](ETHtest.png)
